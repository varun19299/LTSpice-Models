# README

A bunch of useful LTSpice Libraries.

----

## Recommended Usage

Open up the corresponding library with LTSpice, and create a new subcircuit. You may change the symbol if included.

----

## Texas Industries Components

Here's the index of the components that presently work:

[Index Link](http://focus.ti.com/packaged_lits/spice_files/ti_spice_models_index.txt)

## Contribution

Place your library under a suitable hardware folder and send us a pull request.

Can also do so for the icons (.asm) files.

---

## Library List

1. 74XX: Texas Industries manufactured digital gates.
2. MCP6001: Operational Amplifier.
3. LM339: Quad Comparator.

All have been verified in working condition.

----
